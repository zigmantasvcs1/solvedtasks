﻿namespace Task0141
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();

            // Sukuriami darbuotojai ir pridedami į sąrašą...
            var developer = new Developer("Jonas", "Jonaitis", 1);
            developer.AddProgrammingLanguage("C#");
            developer.AddProgrammingLanguage("JavaScript");
            employees.Add(developer);

            var manager = new Manager("Petras", "Petraitis", 2, "IT");
            employees.Add(manager);

            var accountant = new Accountant("Ona", "Onaitė", 3);
            accountant.AddAccount("Sąskaita 1");
            accountant.AddAccount("Sąskaita 2");
            employees.Add(accountant);

            // Atvaizduojami darbuotojai ir atliekami veiksmai
            DisplayAndActOnEmployees(employees);

            // Atnaujinimo pavyzdys
            employees = UpdateEmployee(employees, 1, "Jonas Pakeistas", "Jonaitis Pakeistas");

            // Šalinimo pavyzdys
            employees = RemoveEmployee(employees, 2);

            // Vėl atvaizduojami darbuotojai po atnaujinimo ir šalinimo
            DisplayAndActOnEmployees(employees);
        }

        private static void DisplayAndActOnEmployees(List<Employee> employees)
        {
            foreach (var employee in employees)
            {
                Console.WriteLine(employee.DisplayEmployeeInfo());
                PerformEmployeeSpecificAction(employee);
            }
        }

        private static void PerformEmployeeSpecificAction(Employee employee)
        {
            switch (employee)
            {
                case Developer dev:
                    Console.WriteLine(dev.WriteCode());
                    break;
                case Manager man:
                    Console.WriteLine(man.OrganizeMeeting());
                    Console.WriteLine(man.AssignTask());
                    break;
                case Accountant acc:
                    Console.WriteLine(acc.PrepareFinancialReport());
                    break;
            }
        }

        private static List<Employee> UpdateEmployee(List<Employee> employees, int employeeID, string newName, string newSurname)
        {
            var employee = employees.FirstOrDefault(e => e.EmployeeID == employeeID);

            if (employee != null)
            {
                Employee updatedEmployee;
                switch (employee)
                {
                    case Developer _:
                        updatedEmployee = new Developer(
                            newName,
                            newSurname,
                            employeeID
                        );
                        break;
                    case Manager manager:
                        updatedEmployee = new Manager(
                            newName,
                            newSurname,
                            employeeID,
                            manager.Department
                        );
                        break;
                    case Accountant accountant:
                        updatedEmployee = new Accountant(
                            newName,
                            newSurname,
                            employeeID);
                        // Optionally copy the managed accounts to the new accountant
                        ((Accountant)updatedEmployee).ManagedAccounts = new List<string>(accountant.ManagedAccounts);
                        break;
                    default:
                        return employees; // jokiu update jei tipas nezinomas
                }

                // Remove the old and add the new employee
                employees = employees.Where(e => e.EmployeeID != employeeID).ToList();
                employees.Add(updatedEmployee);

                Console.WriteLine($"Employee {employeeID} updated.");
            }
            else
            {
                Console.WriteLine($"Employee with ID {employeeID} not found.");
            }

            return employees;
        }

        private static List<Employee> RemoveEmployee(List<Employee> employees, int employeeID)
        {
            if (employees.Any(e => e.EmployeeID == employeeID))
            {
                employees = employees.Where(e => e.EmployeeID != employeeID).ToList();
                Console.WriteLine($"Employee {employeeID} removed.");
            }
            else
            {
                Console.WriteLine($"Employee with ID {employeeID} not found.");
            }

            return employees;
        }
    }
}
