﻿namespace Task0141
{
    public class Accountant : Employee
    {
        public List<string> ManagedAccounts { get; set; } = new List<string>();

        public Accountant(
            string name,
            string surname,
            int employeeID)
            : base(
                  name,
                  surname,
                  employeeID)
        {
        }

        public string PrepareFinancialReport()
        {
            return $"{Name} is preparing a financial report.";
        }

        public void AddAccount(string account)
        {
            ManagedAccounts.Add(account);
        }
    }
}
