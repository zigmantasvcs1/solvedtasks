﻿namespace Task0141
{
    public abstract class Employee
    {
        public Employee(
            string name, 
            string surname, 
            int employeeID)
        {
            Name = name;
            Surname = surname;
            EmployeeID = employeeID;
        }

        public string Name { get; }
        public string Surname { get; }
        public int EmployeeID { get; }

        public string DisplayEmployeeInfo()
        {
            return $"Name: {Name}, Surname: {Surname}, ID: {EmployeeID}";
        }
    }
}
