﻿namespace Task0141
{
    public class Developer : Employee
    {
        public List<string> ProgrammingLanguages { get; set; } = new List<string>();

        public Developer(string name, string surname, int employeeID)
            : base(name, surname, employeeID)
        {
        }

        public string WriteCode()
        {
            return $"{Name} is writing code.";
        }

        public void AddProgrammingLanguage(string language)
        {
            ProgrammingLanguages.Add(language);
        }
    }
}
