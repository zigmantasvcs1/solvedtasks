﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task0141
{
    public class Manager : Employee
    {
        public string Department { get; set; }

        public Manager(
            string name,
            string surname,
            int employeeID,
            string department)
            : base(
                  name,
                  surname,
                  employeeID)
        {
            Department = department;
        }

        public string OrganizeMeeting()
        {
            return $"{Name} is organizing a meeting.";
        }

        public string AssignTask()
        {
            return $"{Name} is assigning a task.";
        }
    }
}
